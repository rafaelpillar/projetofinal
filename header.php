<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@1,300&display=swap" rel="stylesheet">
    <title> <?php bloginfo('name') ?> <?php wp_title('|'); ?> </title>

    <?php wp_head(); ?>
</head>
<body>
 <header class="header_container">
    <nav>
        <div class = "container_img_logo"><a href="<?php echo wc_get_page_permalink('home'); ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/LogComesEbebes.png" alt=""></a></div>
        
        <form role="search" method="get" class="inputHeader" action="<?php echo esc_url( home_url( '/' ) ); ?>">
             <img  class = "lupaImg" src="<?php echo get_stylesheet_directory_uri() ?>/images/lupa.png" alt="">
            <label class="screen-reader-text" for="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>"><?php esc_html_e( 'Search for:', 'woocommerce' ); ?></label>
            <input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="search-field" placeholder="<?php echo esc_attr__( 'Search products&hellip;', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
            <button class="hidden" type="submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?>"><?php echo esc_html_x(  'Search', 'submit button', 'woocommerce' ); ?></button>
            <input type="hidden" name="post_type" value="product" />
            </form>
      


             <div class = "actions">
            <a  class = "btn_header">Faça um pedido</a>
            <a><img src="<?php echo get_stylesheet_directory_uri() ?>/images/LogCarrinho.png"alt="" class= "img_carrinho"></a>
            <a href="<?php echo wc_get_page_permalink( 'myaccount' );?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/LogLogin.png"alt="" class= "img_login" ></a>
        </div>
    </nav>
    <div class = "cardCarrinho hidden"></div>
    <div class = "showCarrinho hidden">
        <span id="tituloCarrinho">Carrinho</span>
        <span id="fechar">X</span>

        <hr>
        <br>
        <?php
            global $woocommerce;
            $items = $woocommerce->cart->get_cart(); 


            foreach($items as $item => $values) { 
                $_product =  wc_get_product( $values['data']->get_id()); 
                echo $_product->get_image();
                echo "<b>".$_product->get_title().'</b>';
                echo '<button class = "removeValue">-</button>';
                echo  '<input type ="text"  class = "valorDoProduto" value = ' . $values['quantity'].'>'; 
                echo '<button class = "addValue">+</button>';
                $price = get_post_meta($values['product_id'] , '_price', true);
                echo  '<p> R$ '.$price  .',00</p>' ;
                // echo "Link do produto: ".$_product->get_permalink();
            }
        ?>
        <hr>
        <br>
        <p id = "mostrar-total" >Total do Carrinho:  <b><?php  echo  WC()->cart-> get_cart_total(); ?></b> </p> <br>
        <a id ="comprar-carrinho"   href="<?php echo wc_get_checkout_url(); ?>">Comprar</a>
    </div>
    <script>
        const cart = document.querySelector('.img_carrinho')
        cart.addEventListener('click',(e)=>{
            const show = document.querySelector('.showCarrinho')
            const cardCarrinho = document.querySelector('.cardCarrinho')

            show.classList.toggle("hidden");
            cardCarrinho.classList.toggle("hidden");
        })
        const closeCart = document.querySelector('#fechar')
        closeCart.addEventListener('click',(e)=>{
            const show = document.querySelector('.showCarrinho')
            const cardCarrinho = document.querySelector('.cardCarrinho')

            show.classList.toggle("hidden");
            cardCarrinho.classList.toggle("hidden");
        })

        const addValue = document.querySelector('.addValue')
        addValue.addEventListener('click',(e)=>{
            

        })



    </script>
 </header>