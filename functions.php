<?php 

    function mytheme_add_woocommerce_support() {
        add_theme_support( 'woocommerce' );
    }
    add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

//single product

remove_action('woocommerce_single_product_summary','woocommerce_template_single_price',10);
function woocommerce_template_single_price(){
};

remove_action('woocommerce_after_single_product_summary','woocommerce_output_product_data_tabs',5);
function woocommerce_output_product_data_tabs(){
};

remove_action('woocommerce_after_single_product_summary','woocommerce_template_single_title',5);
function woocommerce_template_single_title(){
};
function changeInputType(){
    echo "<script>document.getElementByClassName('woocommerce-variation-add-to-cart').type = 'text' ;</script>";
}
add_action('wp_head','changeInputType');
// $product->get_description();
// function addDescription(){
//     return $product;
// }
// add_filter('woocommerce_single_product_summary','addDescription',5);
// echo $product->get_description();

// function addPrice(){
//     echo '<p>'.$produc.'</p>';
// };
// add_action('woocommerce_single_product_summary','addPrice',50);




// function addButton(){
//     echo '<div class="buttonSingleProduct"><button>-</button>';
// }
// add_action('woocommerce_before_add_to_cart_quantity','addButton');

// function addButton02(){
//     echo '<button>+</button></div>';
// }
// add_action('woocommerce_after_add_to_cart_quantity','addButton02');
 // 1. Show plus minus buttons
    
 add_action( 'woocommerce_after_quantity_input_field', 'silva_display_quantity_plus' );
    
 function silva_display_quantity_plus() {
 echo '<button type="button" class="plus" >+</button>';
 }
 
 add_action( 'woocommerce_before_quantity_input_field', 'silva_display_quantity_minus' );
 
 function silva_display_quantity_minus() {
 echo '<button type="button" class="minus" >-</button>';
 }
 
 // -------------
 // 2. Trigger update quantity script
 
 add_action( 'wp_footer', 'silva_add_cart_quantity_plus_minus' );
 
 function silva_add_cart_quantity_plus_minus() {
 
 if ( ! is_product() && ! is_cart() ) return;
     
 wc_enqueue_js( "   
         
     $('form.cart,form.woocommerce-cart-form').on( 'click', 'button.plus, button.minus', function() {
 
         var qty = $( this ).parent( '.quantity' ).find( '.qty' );
         var val = parseFloat(qty.val());
         var max = parseFloat(qty.attr( 'max' ));
         var min = parseFloat(qty.attr( 'min' ));
         var step = parseFloat(qty.attr( 'step' ));
 
         if ( $( this ).is( '.plus' ) ) {
             if ( max && ( max <= val ) ) {
             qty.val( max );
             } else {
             qty.val( val + step );
             }
         } else {
             if ( min && ( min >= val ) ) {
             qty.val( min );
             } else if ( val > 1 ) {
             qty.val( val - step );
             }
         }
 
     });
         
 " );
 }





//================================================================

// Shop
    function teste(){?>
        <div class="contentListProducts">
        <h1>selecione uma categoria</h1>
        <div class="categoriasProducts">     
        <?php 
                $categorias_final = get_link_category_img();
                foreach($categorias_final as $category){
                    if($category['name'] != 'Uncategorized'){ ?>
                        <a href="<?php echo $category['link'] ?>">
                        <div class="categoriasListProductsImg" style="background-image:url('<?php echo $category['img']; ?>')" >
                            <div class="categoriasListProductsLink"><?php echo $category['name'] ?></div>
                        </div>
                        </a>
                    <?php
                    }
                };

                ?>  
        </div>
        <?php
    };
    add_action( 'woocommerce_before_shop_loop', 'teste' );
    function woocommerce_breadcrumb (){

    }
    remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb',20 );

    

    function teste02(){?>
        <div class= "conteinerListText">
        <p class="pratosList">Pratos</p>
        <p class="foodList">Comida </p>
        </div>        
        <?php
    };
    add_action( 'woocommerce_before_shop_loop', 'teste02' );

    
    function teste03(){?>

        <form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
            <label class="screen-reader-text" for="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>"><?php esc_html_e( 'Search for:', 'woocommerce' ); ?></label>
            <input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="search-field" placeholder="<?php echo esc_attr__( 'Search products&hellip;', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
            <button type="submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?>"><?php echo esc_html_x( 'Search', 'submit button', 'woocommerce' ); ?></button>
            <input type="hidden" name="post_type" value="product" />
</form>
    <?php
    };
    add_action('woocommerce_before_shop_loop','teste03');


    function teste04(){?>
        <?php do_action( 'woocommerce_widget_price_filter_start', $args ); ?>

        <form method="get" action="<?php echo esc_url( $form_action ); ?>">
            <div class="price_slider_wrapper">
                <div class="price_slider" style="display:none;"></div>
                <div class="price_slider_amount" data-step="<?php echo esc_attr( $step ); ?>">
                    <input type="text" id="min_price" name="min_price" value="<?php echo esc_attr( $current_min_price ); ?>" data-min="<?php echo esc_attr( $min_price ); ?>" placeholder="<?php echo esc_attr__( 'Min price', 'woocommerce' ); ?>" />
                    <input type="text" id="max_price" name="max_price" value="<?php echo esc_attr( $current_max_price ); ?>" data-max="<?php echo esc_attr( $max_price ); ?>" placeholder="<?php echo esc_attr__( 'Max price', 'woocommerce' ); ?>" />
                    <?php /* translators: Filter: verb "to filter" */ ?>
                    <button type="submit" class="button"><?php echo esc_html__( 'Filter', 'woocommerce' ); ?></button>
                    <div class="price_label" style="display:none;">
                        <?php echo esc_html__( 'Price:', 'woocommerce' ); ?> <span class="from"></span> &mdash; <span class="to"></span>
                    </div>
                    <?php echo wc_query_string_form_fields( null, array( 'min_price', 'max_price', 'paged' ), '', true ); ?>
                    <div class="clear"></div>
                </div>
            </div>
        </form>

        <?php do_action( 'woocommerce_widget_price_filter_end', $args ); ?>
<?php    
}
add_action('woocommerce_before_shop_loop','teste04');

   function teste05(){?>
    <?php
   }
add_action('woocommerce_shop_loop','teste05');



//===========================================================================

    function format_products($products){
        $products_final = [];
        foreach($products as $product) {
            $image_id = $product->get_image_id();
            $image_url = wp_get_attachment_image_src($image_id, 'medium')[0];
            $products_final[] = [
                'name' => $product->get_name(),
                'price'=>$product->get_price(),
                'link'=>$product->get_permalink(),
                 'img' => $product->get_image(),
                 'img_url'=>$image_url,
                // 'img'=>$product->get_image(),
            ];
        };

        return $products_final;
    };





    function get_link_category_img(){
        $taxonomy     = 'product_cat';
        $orderby      = 'name';
        $show_count   = 0;      // 1 for yes, 0 for no
        $pad_counts   = 0;      // 1 for yes, 0 for no
        $hierarchical = 1;      // 1 for yes, 0 for no
        $title        = '';
        $empty        = 0;

        $args = array(
               'taxonomy'     => $taxonomy,
               'orderby'      => $orderby,
               'show_count'   => $show_count,
               'pad_counts'   => $pad_counts,
               'hierarchical' => $hierarchical,
               'title_li'     => $title,
               'hide_empty'   => $empty
        );
        $categorias_lista = [];
        $categories = get_categories($args);
        if ($categories){
            foreach($categories as $category){
                $category_id = $category->term_id;
                $img_id = get_term_meta($category_id, 'thumbnail_id', true);
                $categorias_lista[] = [
                    'name' => $category->name,
                    'id'=> $category_id,
                    'link'=> get_term_link($category_id, 'product_cat'),
                    'img'=> wp_get_attachment_image_src($img_id, 'slide')[0],
                ];

            };
        };
        return $categorias_lista;
    }



?>