<?php
// Template name:Principal 
 get_header(); ?>
 <div class="divPrincipal">
        <h1>Comes&Bebes</h1>
        <p>O resturante para todas as fomes</p>
</div>

<div class="ContentPrincipal">
<h1>Conheça Nossa Loja</h1>

<p class="mainPratos" >Tipos de pratos Principais</p>
<div class="categorias">     
<?php 
            $categorias_final = get_link_category_img();
            foreach($categorias_final as $category){
                if($category['name'] != 'Uncategorized'){ ?>
                   <div class="categoriasImg" style="background-image:url('<?php echo $category['img']; ?>')" >
                        <div class="categoriasLink"><a href="<?php echo $category['link'] ?>"><?php echo $category['name'] ?></a></div>
                    </div>
                <?php
                }
            };
        ?>
</div>
    <div class="containerText">
    <p class="pratosParagrafo">Pratos do dia de hoje:</p>
    <p class="Dias"><?php
            date_default_timezone_set("America/Sao_Paulo");
            $diaDaSemana = date('l');
            switch ($diaDaSemana) {
            case 'Monday':
                echo "SEGUNDA";
                break;
            case 'Tuesday':
                echo "TERÇA";
                break;
            case 'Wednesday':
                echo "QUARTA";
                break;
            case 'Thursday':
                echo "QUINTA";
                break;
            case 'Friday':
                echo "SEXTA";
                break;
            case 'Saturday':
                echo "SÁBADO";
                break;
            case 'Sunday':
                echo "DOMINGO";
                break;
            default:
                echo "O mundo acabou, pois hoje nao é dia da semana";
            }
        ?></p>
    </div>
    <div class="pratos">
    <?php
        $products = wc_get_products([
            'limit' =>4,
            'tag' =>date('l'),
        ]);
        $products_formatado = format_products($products);

        foreach($products_formatado as $product) { ?>
            <div class="prato-atual" style = "background-image: url('<?php echo $product['img_url']; ?>')">
                <div class="container-infos">
                    <p class="nome-do-dia"><?php echo $product['name']; ?></p>
                    <p class="preco-do-dia">R$<?php echo $product['price']; ?></p>
                    <a href="<?php echo $product['link'] ?>" class="adicionar-carrinho"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/CarrinhoBtn.png" alt=""></a>
                </div>
            </div>
        <?php }
    ?>
    </div>

<div class="containerButton">
    <a href="" class="buttonOptions" >Veja outra opções</a>
</div>

</div>

<div class="footerStyle">
    <div class = "inf">
        <h1>VISITE NOSSA LOJA FÍSICA</h1>
        <div class="mapouter"><div class="gmap_canvas"><iframe width="345" height="203" id="gmap_canvas" src="https://maps.google.com/maps?q=2880%20Broadway,%20New%20York&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://123movies-to.org"></a><br><style>.mapouter{position:relative;text-align:right;height:203px;width:345px;}</style><a href="https://www.embedgooglemap.net">embedgooglemap.net</a><style>.gmap_canvas {overflow:hidden;background:none!important;height:203px;width:345px;}</style></div></div>
        <p> <img src="<?php echo get_stylesheet_directory_uri() ?>/images/garfo.png" class = "imagemAdress" />Rua lorem ipsum, 123, LI, Brasil</p>
        <p> <img src="<?php echo get_stylesheet_directory_uri() ?>/images/phone.png"  class = "imagemAdress"/>(XX) XXXX-XXXX</p>
    </div>
    <div class="containerSlider">
    <ul class="slider">
    <li>
          <input type="radio" id="slide1" name="slide" checked>
          <label for="slide1"></label>
          <img src="<?php echo get_stylesheet_directory_uri() ?>/images/slider01.png" />
    </li>
    <li>
          <input type="radio" id="slide2" name="slide">
          <label for="slide2"></label>
          <img src="<?php echo get_stylesheet_directory_uri() ?>/images/slider02.png" />
    </li>
    <li>
          <input type="radio" id="slide3" name="slide">
          <label for="slide3"></label>
          <img src="<?php echo get_stylesheet_directory_uri() ?>/images/slider03.png" />
    </li>
    </ul>
    </div>
</div>
<?php get_footer(); ?>